const path = require('path')
const fs = require('fs');
const commandExist = require('command-exists').sync;
const exec = require('teen_process').exec;
const portscanner = require('portscanner');
const copy = require('copy-dir');

const hubPort = 4444;
let mongoPort;
let apiPort;

const mongoDataPath = '/data/db';

function getInstallationPath() {
    const installLocation = path.join(__dirname, '../demo');
    if (!fs.existsSync(installLocation)) {
        fs.mkdirSync(installLocation);
    }
    return installLocation;
}

function getHubPort() {
    return hubPort;
}

function isMongoInstalled() {
    return commandExist('mongod');
}

function getAvailablePort() {
    return portscanner.findAPortNotInUse(3000, 4000);
}

async function installMongo() {
    if (isMongoInstalled()) {
        console.log('Mongo already installed')
        return;
    }

    try {
        console.log('Installing Mongo db ...')
        switch (process.platform) {
            case 'darwin':
                await exec('brew', ['install', 'mongodb']);
                break;
            case 'windows':
                throw new Error('Implementation for windows pending');
                break;
            case 'linux':
                await exec('apt-get', ['install', '-y', 'mongodb-org']);
                break;
            default:
                throw new Error('Mongo install error : Unknown OS')
        }
    } catch (e) {
        console.log(e.stderr);
    }

}

async function startMongo() {
    const dataDir = `${getInstallationPath()}${mongoDataPath}`;
    console.log(dataDir)
    mongoPort = await getAvailablePort();

    if (!fs.existsSync(dataDir)) {
        fs.mkdirSync(path.dirname(dataDir));
        fs.mkdirSync(dataDir);
    }

    try {
        console.log(`Mongo db starting at port ${mongoPort}`)
        await exec('mongod', ['--dbpath', `${dataDir}`, '--port', `${mongoPort}`])
    } catch (e) {
        console.log(e.stderr);
    }
}


async function setup() {
    try {
        await exec('npm', ['install'])
    } catch (e) {
        console.log(e.stderr);
    }
}

async function startAPI() {
    const source = path.join(__dirname, '../db-operations-api');
    const dest = path.join(__dirname, '../demo/db-operations-api')
    apiPort = await getAvailablePort();
    copy.sync(source, dest)
    try {
        await exec('npm', ['install'], { cwd: dest })
    } catch (e) {
        console.log(e.stderr);
    }
    try {
        await exec('export', [`PORT=${apiPort}`], { cwd: dest })
    } catch (e) {
        console.log(e.stderr);
    }
    try {
        console.log(`API running on port : ${apiPort}`)
        await exec('npm', ['run', 'start'], { cwd: dest, detach: true })
    } catch (e) {
        console.log(e.stderr);
    }
}

startAPI();